﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsTerm.ViewModels
{
    public class UserRoles
    {
        public string Role { get; set; }
        public string User { get; set; }
    }
}