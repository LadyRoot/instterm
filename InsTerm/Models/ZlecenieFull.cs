﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsTerm.Models
{
    public class ZlecenieFull
    {
        public long id { get; set; } // zlecenie ID
        public string info { get; set; }
        public string name { get; set; } // Merchant
        public string street { get; set; }
        public string street2 { get; set; }
        public string city { get; set; }
        public string phone { get; set; }
        public string phone2 { get; set; }
        public string email { get; set; }
        public string terminalTID { get; set; }
        public string terminalSerial { get; set; }
        public string terminalModel { get; set; }
        public string terminalDescription { get; set; }
        public string simImei { get; set; }
        public string simDesc { get; set; }
        public string installDate { get; set; }
        public string status { get; set; }
    }
}