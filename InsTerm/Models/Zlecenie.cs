﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsTerm.Models
{
    public class Zlecenie
    {
        public long id { get; set; }
        public string name { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string phone { get; set; }
        public string terminal { get; set; }
        public string installDate { get; set; }
        public string status { get; set; }
    }
}