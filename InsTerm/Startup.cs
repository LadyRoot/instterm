﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InsTerm.Startup))]
namespace InsTerm
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
        }
    }
}
