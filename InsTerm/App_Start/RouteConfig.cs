﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace InsTerm
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.AppendTrailingSlash = true;
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{id2}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, id2 = UrlParameter.Optional }
            );
            routes.MapRoute(
                "ManageUser",
                "Administrator/ManageUser/{id}/{id2}",
                   new
                   {
                       controller = "Administrator",
                       action = "ManageUser",
                       subscriber = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                   }
                );

        }
    }
}
