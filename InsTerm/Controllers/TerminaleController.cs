﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InsTerm.Controllers
{
    [Authorize(Roles = "Operacje,Administrator")]
    public class TerminaleController : Controller
    {
        private DB_Entities _db = new DB_Entities();

        // GET: Terminale
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult List()
        {
            var model = _db.TERMINAL.ToList();
            return View(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            var modele = (from m in _db.TERMINAL_MODEL
                         select m).ToList();
            SelectList list = new SelectList(modele, "TM_ID", "TM_NAME");
            ViewBag.T_MODEL = list;
            return View();
        }
        [HttpPost]
        public ActionResult Create([Bind(Include = "T_ID, T_MODEL, T_SERIAL, T_NOTES")] TERMINAL terminal)
        {
            int exists = _db.TERMINAL.Where(t => t.T_ID == terminal.T_ID).Count();
            if (exists > 0)
            {
                TempData["Error"] = "Terminal o podanym numerze TID już istnieje!";
                return RedirectToAction("Create");
            }

            if (ModelState.IsValid)
            {
                _db.TERMINAL.Add(terminal);
                _db.SaveChanges();
                return RedirectToAction("List");
            }
            TempData["Error"] = "Błędne dane!";
            return View(terminal);
        }


        // MODELE

        public ActionResult ModelList()
        {
            var model = _db.TERMINAL_MODEL.ToList();
            return View(model);
        }
        public ActionResult ModelDetails(int? id)
        {
            if (id == null) return RedirectToAction("ModelList");

            var model = _db.TERMINAL_MODEL.Where(t => t.TM_ID == id).First();
            if (model == null) return RedirectToAction("ModelList");


            return View(model);
        }
        [HttpGet]
        public ActionResult AddModel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddModel([Bind(Include = "TM_NAME, TM_DESC, TM_VERSION")] TERMINAL_MODEL terminalModel)
        {
            try
            {
                int newId = _db.TERMINAL_MODEL.Max(m => m.TM_ID);
                terminalModel.TM_ID = newId + 1;
            }
            catch (Exception ex)
            {
                ViewBag.Errors = "Nie udało się pobrać modelu";
                return View();
            }
            if (ModelState.IsValid)
            {
                _db.TERMINAL_MODEL.Add(terminalModel);
                _db.SaveChanges();
                return RedirectToAction("ModelList");
            }
            return View();
        }

        [HttpGet]
        public ActionResult EditModel(int? id)
        {
            if (id == null) return RedirectToAction("ModelList");

            var model = _db.TERMINAL_MODEL.Where(m => m.TM_ID == id).First();
            if (model == null) return RedirectToAction("ModelList");
            return View(model);
        }

        [HttpPost]
        public ActionResult EditModel([Bind(Include = "TM_ID, TM_NAME, TM_DESC, TM_VERSION")] TERMINAL_MODEL terminalModel)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(terminalModel).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("ModelList");
            }
            return View(terminalModel);
        }



        [HttpGet]
        public ActionResult DeleteModel(int? id)
        {
            var terminalModel = _db.TERMINAL_MODEL.Where(t => t.TM_ID == id).First();
            if (terminalModel == null)
            {
                TempData["Error"] = "Nie można usunąć modelu. Brak terminalu o takim numerze ID.";
                return View();
            }
            return View(terminalModel);
        }

        [HttpPost]
        public ActionResult DeleteModelConfirmed(FormCollection collection)
        {
            string modelId = collection.Get("TM_ID");
            if (String.IsNullOrEmpty(modelId))
            {
                TempData["Error"] = "Nie można usunąć - nie znaleziono urządzenia!";
                return RedirectToAction("ModelList");
            }
            int tid = Int32.Parse(modelId);
            int zl = _db.TERMINAL.Where(z => z.T_MODEL == tid).Count();
            if (zl > 0)
            {
                TempData["Error"] = "Nie można usunąć. Urządzenie przypisane jest już do co najmniej jednego terminalu.";
                return RedirectToAction("ModelList");
            }

            var terminalModelToDelete = _db.TERMINAL_MODEL.Where(t => t.TM_ID == tid).First();
            _db.Entry(terminalModelToDelete).State = EntityState.Deleted;
            _db.SaveChanges();
            return RedirectToAction("ModelList");
        }


        //public ActionResult Aplikacje()
        //{
        //    var model = _db.TERMINAL_APP.ToList();
        //    return View(model);
        //}


        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null) return RedirectToAction("List");

            var terminal = _db.TERMINAL
                .Where(t => t.T_ID == id).First();
            ViewBag.T_MODEL = new SelectList(_db.TERMINAL_MODEL, "TM_ID", "TM_NAME", terminal.T_MODEL);
            return View(terminal);
        }


        [HttpPost]
        public ActionResult Edit([Bind(Include = "T_ID, T_MODEL, T_SERIAL, T_NOTES")] TERMINAL terminal)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(terminal).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Details", new { id = terminal.T_ID });
            }
            return View();
        }








        public ActionResult Details(int? id)
        {
            if (id == null) return RedirectToAction("List");

            var terminal = _db.TERMINAL
                .Include(t => t.TERMINAL_MODEL)
                .Where(t => t.T_ID == id).First();
            ViewBag.T_MODEL = new SelectList(_db.TERMINAL_MODEL, "TM_ID", "TM_NAME", terminal.T_MODEL);
            return View(terminal);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            var terminal = _db.TERMINAL.Where(t => t.T_ID == id).First();
            if (terminal == null)
            {
                ViewBag.Error = "Nie można usunąć terminalu. Brak terminalu o takim numerze TID.";
                return View();
            }
            return View(terminal);
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(FormCollection collection)
        {
            string terminalId = collection.Get("TID");
            if (String.IsNullOrEmpty(terminalId))
            {
                TempData["Error"] = "Nie można usunąć - nie znaleziono terminalu!";
                return RedirectToAction("List");
            }
            int tid = Int32.Parse(terminalId);
            int zl = _db.ZLECENIE.Where(z => z.TERMINAL.T_ID == tid).Count();
            if (zl > 0)
            {
                TempData["Error"] = "Nie można usunąć. Do terminalu przypisane jest już zlecenie.";
                return RedirectToAction("List");
            }

            var terminalToDelete = _db.TERMINAL.Where(t => t.T_ID == tid).First();
                _db.Entry(terminalToDelete).State = EntityState.Deleted;
                _db.SaveChanges();
                return RedirectToAction("List");



        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}