﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InsTerm.ViewModels;

namespace InsTerm.Controllers
{
    [Authorize(Roles ="Operacje,Administrator")]
    public class AdministratorController : Controller
    {
        private DB_Entities db = new DB_Entities();


        // GET: Administrator
        public ActionResult Index()
        {
            if (User.IsInRole("Administrator"))
            {
                var userRoles = db.v_UserRoles.OrderBy(r => r.USERNAME).ToList();
                return View(userRoles);
            }
            else
            {
                var userRoles = db.v_UserRoles.Where(r => r.ROLENAME != "Administrator" && r.ROLENAME != "Operacje").OrderBy(r => r.USERNAME).ToList();
                return View(userRoles);
            }
        }

        [HttpGet]
        public ActionResult ManageUser(string id)
        {
            if (id.Equals("") || id == null) return RedirectToAction("Index");
            var user = db.AspNetUsers
                .Where(u => u.UserName.Equals(id)).FirstOrDefault();

            if (user == null)
            {
                ViewBag.Error = "Nie znaleziono użytkownika.";
            }
            ViewBag.userName = id;

            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageUser([Bind(Include = "UserName,Email,EmailConfirmed,PhoneNumber,PhoneNumberConfirmed,PasswordHash,SecurityStamp,TwoFactorEnabled,LockoutEnabled,LockoutEndDateUtc,AccessFailedCount")] AspNetUsers user)
        {
            AspNetUsers originalUser = db.AspNetUsers.First(u => u.UserName == user.UserName);
            originalUser.Email = user.Email;
            originalUser.EmailConfirmed = true;
            originalUser.LockoutEndDateUtc = user.LockoutEndDateUtc;
            originalUser.PhoneNumber = user.PhoneNumber;
            originalUser.PhoneNumberConfirmed = true;

            db.Entry(originalUser).State = EntityState.Modified;            

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message.ToString();
                return View();
            }


            return RedirectToAction("Details", new { id = user.UserName });
        }



        public ActionResult Details(string id)
        {
            var user = db.AspNetUsers
                        .Include(r => r.AspNetRoles)
                        .Where(r => r.UserName == id)
                        .First();
            return View(user);
        }


        // GET: Administrator/AssignRole/Użyszkodnik
        [HttpGet]
        public ActionResult AssignRole(string id)
        {
            if (String.IsNullOrEmpty(id)) return View();
            if (User.IsInRole("Administrator"))
            {
                var roles = (from r in db.AspNetRoles
                             select r).ToList();
                SelectList list = new SelectList(roles, "Id", "Name");
                ViewBag.U_NAME = id;
                ViewBag.Roles = list;
                return View();
            }
            else
            {
                var roles = (from r in db.AspNetRoles
                             where r.Name != "Administrator" && r.Name != "Operacje"
                             select r).ToList();
                SelectList list = new SelectList(roles, "Id", "Name");
                ViewBag.U_NAME = id;
                ViewBag.Roles = list;
                return View();
            }
              
           

        }


        [HttpPost]
        public ActionResult AssignRole(FormCollection collection)
        {
            string username = collection.Get("U_NAME");
            string role = collection.Get("Roles");
            if (!String.IsNullOrEmpty(username))
            {
                var user = db.AspNetUsers.Where(u => u.UserName == username).First();
                var newRole = db.AspNetRoles.Where(r => r.Id == role).First();
                try
                {
                    user.AspNetRoles.Clear();
                    user.AspNetRoles.Add(newRole);
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.Message.ToString();
                    return View();
                }

            }
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}