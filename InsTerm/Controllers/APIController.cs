﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using InsTerm.Models;

namespace InsTerm.Controllers
{


    public class APIController : ApiController
    {
        private DB_Entities _db = new DB_Entities();

        private const string CacheKey = "ZleceniaCacheStore";


        public IEnumerable<ZlecenieFull> Get(string id)
        {
            if (string.IsNullOrEmpty(id)) return null;
            long key = long.Parse(id);
            string userName;
            try
            {
                var user = _db.USERKEYS.Where(k => k.K_ID == key).First();
                if (user == null) return null;
                userName = user.K_USERNAME;
            }
            catch (Exception ex)
            {
                return null;
            }


            var zlecenia = _db.ZLECENIE
                .Where(z => z.ZL_USER == userName).ToList();
            if (zlecenia == null || zlecenia.Count == 0) return null;

            List<ZlecenieFull> lst = new List<ZlecenieFull>();
            foreach (ZLECENIE z in zlecenia)
            {
                ZlecenieFull zl = new ZlecenieFull();
                zl.id = z.ZL_ID;
                zl.info = z.ZL_INFO;
                zl.name = z.MERCHANT.M_NAME;
                zl.street = z.MERCHANT.M_STREET;
                zl.street2 = z.MERCHANT.M_STREET2;
                zl.city = z.MERCHANT.M_CITY;
                zl.phone = z.MERCHANT.M_PHONE1;
                zl.phone2 = z.MERCHANT.M_PHONE2;
                zl.email = z.MERCHANT.M_EMAIL;
                zl.terminalTID = z.ZL_TID.ToString();
                zl.terminalSerial = z.TERMINAL.T_SERIAL;
                zl.terminalDescription = z.TERMINAL.T_NOTES;
                zl.terminalModel = z.TERMINAL.TERMINAL_MODEL.TM_NAME;
                zl.simImei = z.SIM.SIM_IMEI;
                zl.simDesc = z.SIM.SIM_NETWORK;
                zl.installDate = z.ZL_INSTALLDATE.ToString();
                zl.status = z.ZL_STATUS;

                lst.Add(zl);
                
            }
            return lst;
        }

        // GET: api/API/5
        public ZlecenieFull Get(string id, string id2)
        {
            if (string.IsNullOrEmpty(id)) return null;
            long key = long.Parse(id);
            long zlId = long.Parse(id2);
            string userName;
            try
            {
                var user = _db.USERKEYS.Where(k => k.K_ID == key).First();
                if (user == null) return null;
                userName = user.K_USERNAME;
            }
            catch (Exception ex)
            {
                return null;
            }


            var zlecenie = _db.ZLECENIE
                .Where(z => z.ZL_USER == userName && z.ZL_ID == zlId).First();
            if (zlecenie == null) return null;

                ZlecenieFull zl = new ZlecenieFull();
                zl.id = zlecenie.ZL_ID;
                zl.info = zlecenie.ZL_INFO;
                zl.name = zlecenie.MERCHANT.M_NAME;
                zl.street = zlecenie.MERCHANT.M_STREET;
                zl.street2 = zlecenie.MERCHANT.M_STREET2;
                zl.city = zlecenie.MERCHANT.M_CITY;
                zl.phone = zlecenie.MERCHANT.M_PHONE1;
                zl.phone2 = zlecenie.MERCHANT.M_PHONE2;
                zl.email = zlecenie.MERCHANT.M_EMAIL;
                zl.terminalTID = zlecenie.ZL_TID.ToString();
                zl.terminalSerial = zlecenie.TERMINAL.T_SERIAL;
                zl.terminalDescription = zlecenie.TERMINAL.T_NOTES;
                zl.terminalModel = zlecenie.TERMINAL.TERMINAL_MODEL.TM_NAME;
                zl.simImei = zlecenie.SIM.SIM_IMEI;
                zl.simDesc = zlecenie.SIM.SIM_NETWORK;
                zl.installDate = zlecenie.ZL_INSTALLDATE.ToString();
                zl.status = zlecenie.ZL_STATUS;



            return zl;
        }

        // POST: api/API
        public bool Post(string id, string id2, string id3, string id4)
        {
            // if not bad request
            if (!(String.IsNullOrEmpty(id) && String.IsNullOrEmpty(id2) && String.IsNullOrEmpty(id3)))
            {
                long key, zlId;
                try
                {
                    key = long.Parse(id);       // sparsuj UQKey
                    zlId = long.Parse(id2);     // sparsuj ZL_ID
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                    return false;
                }


                var user = _db.USERKEYS.Where(uk => uk.K_ID == key).First();
                if (user == null) return false;

                var zlecenie = _db.ZLECENIE.Where(z => z.ZL_ID == zlId && z.ZL_USER == user.K_USERNAME).First();
                if (zlecenie == null) return false;

                // id3 - status
                if (id3 == "N" || id3 == "p" || id3 == "f" || id3 == "a" || id3 == "u")
                {
                    zlecenie.ZL_STATUS = id3;
                }
                else
                {
                    return false;
                }

                // id4 - info
                if (!String.IsNullOrEmpty(id4))
                {
                    zlecenie.ZL_INFO = id4;
                }

                try
                {
                    _db.Entry(zlecenie).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                    return false;
                }


            }

            return false;
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
