﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace InsTerm.Controllers
{
    [Authorize(Roles ="Operacje,Administrator,Instalator")]
    public class ZLECENIEController : Controller
    {
        private DB_Entities db = new DB_Entities();

        // GET: ZLECENIE
        public ActionResult Index()
        {
            if (User.IsInRole("Instalator"))
            {
                var zLECENIE = db.ZLECENIE
                        .Include(z => z.MERCHANT)
                        .Include(z => z.SIM)
                        .Include(z => z.TERMINAL)
                        .Where(z => z.ZL_USER == User.Identity.Name)
                        .OrderByDescending(z => z.ZL_INSTALLDATE).ToList();
                return View(zLECENIE);
            }
            else
            {
                var zLECENIE = db.ZLECENIE
                        .Include(z => z.MERCHANT)
                        .Include(z => z.SIM)
                        .Include(z => z.TERMINAL)
                        .OrderByDescending(z => z.ZL_INSTALLDATE);
                return View(zLECENIE.ToList());
            }

        }

        // GET: ZLECENIE/Details/5
        public ActionResult Details(long? id)
        {
            ZLECENIE zLECENIE;


            if (User.IsInRole("Instalator"))
            {
                zLECENIE = db.ZLECENIE
                    .Include(z => z.MERCHANT)
                    .Include(z => z.SIM)
                    .Include(z => z.PERIPHERALS)
                    .Include(z => z.TERMINAL)
                    .Include(z => z.TERMINAL_APP)
                    .Where(z => z.ZL_ID == id && z.ZL_USER == User.Identity.Name)
                    .FirstOrDefault();
            }
            else
            {
                zLECENIE = db.ZLECENIE
                    .Include(z => z.MERCHANT)
                    .Include(z => z.SIM)
                    .Include(z => z.PERIPHERALS)
                    .Include(z => z.TERMINAL)
                    .Include(z => z.TERMINAL_APP)
                    .Where(z => z.ZL_ID == id)
                    .FirstOrDefault();
            }


            if (zLECENIE == null)
            {
                return RedirectToAction("Index");
            }
            return View(zLECENIE);
        }

        // GET: ZLECENIE/Create
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Create()
        {


            var roles = (from r in db.v_UserRoles
                         where r.ROLENAME=="Instalator"
                         select r).ToList();
            SelectList list = new SelectList(roles, "USERNAME", "USERNAME");

            var terminale = (from t in db.TERMINAL
                             where t.ZLECENIE.Count() == 0
                             select t).ToList();
            SelectList terminalList = new SelectList(terminale, "T_ID", "T_SERIAL");

            var karty = (from s in db.SIM
                         where s.ZLECENIE.Count() == 0
                         select s).ToList();
            SelectList simList = new SelectList(karty, "SIM_IMEI", "SIM_IMEI");

            ViewBag.ZL_MID = new SelectList(db.MERCHANT, "M_ID", "M_NAME");
            ViewBag.ZL_SIM = simList;
            ViewBag.ZL_TID = terminalList;
            ViewBag.ZL_APP = new SelectList(db.TERMINAL_APP, "TA_ID", "TA_NAME");
            ViewBag.ZL_USER = list;
            return View();
        }

        // POST: ZLECENIE/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Create(FormCollection collection)
        {
            string frmTID = collection.Get("ZL_TID");


            string frmMID = collection.Get("ZL_MID");

            DateTime frmInstallDate = DateTime.Parse(collection.Get("ZL_INSTALLDATE"));
            string frmINFO = collection.Get("ZL_INFO");
            //ZL_STATUS

            string frmUser = collection.Get("ZL_USER");
            var user = db.AspNetUsers.Where(u => u.UserName == frmUser).First();
            if (user == null) return View();

            string frmSIM = collection.Get("ZL_SIM");

            ZLECENIE z = new ZLECENIE();

            long? newId = db.ZLECENIE.Max(zl => (long?)zl.ZL_ID);
            if (newId == null || newId == 0)
            { newId = 1; }
            else
            { newId = newId + 1; }
            z.ZL_ID = (long)newId;
            z.ZL_INFO = frmINFO;
            z.ZL_INSTALLDATE = frmInstallDate;
            z.ZL_MID = Int32.Parse(frmMID);
            z.ZL_STATUS = "N";
            z.ZL_SIM = frmSIM;
            z.ZL_USER = frmUser;
            z.ZL_TID = Int32.Parse(frmTID);

            try
            {
                db.ZLECENIE.Add(z);
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message.ToString();
                return View();
            }

            return RedirectToAction("Details", new { id = z.ZL_ID });
        }

        // GET: ZLECENIE/Edit/5
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZLECENIE zLECENIE = db.ZLECENIE.Find(id);
            if (zLECENIE == null)
            {
                return RedirectToAction("Index");
            }


            var terminale = (from t in db.TERMINAL
                             where t.ZLECENIE.Count() == 0 || t.T_ID == id
                             select t).ToList();
            SelectList terminalList = new SelectList(terminale, "T_ID", "T_SERIAL");

            var karty = (from s in db.SIM
                         where s.ZLECENIE.Count() == 0 || s.SIM_IMEI == zLECENIE.ZL_SIM
                         select s).ToList();
            SelectList simList = new SelectList(karty, "SIM_IMEI", "SIM_IMEI");

            var instalatorzy = (from i in db.v_UserRoles 
                                where i.ROLENAME == "Instalator"
                                select i).ToList();
            SelectList instList = new SelectList(instalatorzy, "USERNAME", "USERNAME");

            SelectList statusList = new SelectList(
                new List<SelectListItem>
                {
                                new SelectListItem { Text = "Nowe (n)", Value = "n"},
                                new SelectListItem { Text = "W trakcie Przetwarzania (p)", Value = "p"},
                                new SelectListItem { Text = "Zakończone - udane (f)", Value = "f"},
                                new SelectListItem { Text = "Zakończone - nieudane (a)", Value = "a"},
                                new SelectListItem { Text = "Inne (u)", Value = "u"},
                }, "Value", "Text");


            ViewBag.ZL_MID = new SelectList(db.MERCHANT, "M_ID", "M_NAME", zLECENIE.ZL_MID);
            ViewBag.ZL_SIM = simList;
            ViewBag.ZL_TID = terminalList;
            ViewBag.ZL_USER = instList;
            ViewBag.ZL_STATUS = statusList;
            return View(zLECENIE);
        }

        // POST: ZLECENIE/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Edit(FormCollection collection)
        {
            int id = int.Parse(collection.Get("ZL_ID"));
            int tid = int.Parse(collection.Get("ZL_TID"));
            int mid = int.Parse(collection.Get("ZL_MID"));
            DateTime dte = DateTime.Parse(collection.Get("ZL_INSTALLDATE"));
            string info = collection.Get("ZL_INFO");
            string status = collection.Get("ZL_STATUS");

            string instuser = collection.Get("ZL_USER");

            string simImei = collection.Get("ZL_SIM");

            ZLECENIE zLECENIE = db.ZLECENIE.Where(z => z.ZL_ID == id).First();
            if (zLECENIE == null) return RedirectToAction("Index");

            zLECENIE.ZL_INFO = info;
            zLECENIE.ZL_INSTALLDATE = dte;
            zLECENIE.ZL_MID = mid;
            zLECENIE.ZL_SIM = simImei;
            zLECENIE.ZL_STATUS = status;
            zLECENIE.ZL_TID = tid;
            zLECENIE.ZL_USER = instuser;

            try
            { 
                db.Entry(zLECENIE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { id = zLECENIE.ZL_ID });
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message.ToString();
            }

            var terminale = (from t in db.TERMINAL
                             where t.ZLECENIE.Count() == 0 || t.T_ID == id
                             select t).ToList();
            SelectList terminalList = new SelectList(terminale, "T_ID", "T_SERIAL");

            var karty = (from s in db.SIM
                         where s.ZLECENIE.Count() == 0 || s.SIM_IMEI == zLECENIE.ZL_SIM
                         select s).ToList();
            SelectList simList = new SelectList(karty, "SIM_IMEI", "SIM_IMEI");

            var instalatorzy = (from i in db.v_UserRoles
                                where i.ROLENAME == "Instalator"
                                select i).ToList();
            SelectList instList = new SelectList(instalatorzy, "USERNAME", "USERNAME");


            ViewBag.ZL_MID = new SelectList(db.MERCHANT, "M_ID", "M_NAME", zLECENIE.ZL_MID);
            ViewBag.ZL_SIM = simList;
            ViewBag.ZL_TID = terminalList;
            ViewBag.ZL_USER = instList;
            return View(zLECENIE);
        }

        [Authorize(Roles = "Instalator")]
        public ActionResult EditOwn(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZLECENIE zLECENIE = db.ZLECENIE.Find(id);
            if (zLECENIE == null)
            {
                TempData["Error"] = "Nie znaleziono takiego zlecenia.";
                return RedirectToAction("Index");
            }

            SelectList statusList = new SelectList(
                new List<SelectListItem>
                {
                    new SelectListItem { Text = "Nowe (n)", Value = "n"},
                    new SelectListItem { Text = "W trakcie Przetwarzania (p)", Value = "p"},
                    new SelectListItem { Text = "Zakończone - udane (f)", Value = "f"},
                    new SelectListItem { Text = "Zakończone - nieudane (a)", Value = "a"},
                    new SelectListItem { Text = "Inne (u)", Value = "u"},
                }, "Value", "Text");

            ViewBag.ZL_ST = statusList;
            return View(zLECENIE);
        }

        // POST: ZLECENIE/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Instalator")]
        public ActionResult EditOwn(FormCollection collection)
        {
            long zlId = long.Parse(collection.Get("ZL_ID"));
            string zlInfo = collection.Get("ZL_INFO");
            string zlStatus = collection.Get("ZL_ST");

            var zlecenie = db.ZLECENIE.Where(z => z.ZL_ID == zlId).First();
            if (zlecenie == null)
            {
                TempData["Error"] = "Wystąpił błąd! Zlecenie nie zostało znalezione!";
                return RedirectToAction("Index");
            }
            zlecenie.ZL_INFO = zlInfo;
            zlecenie.ZL_STATUS = zlStatus;
            try
            {
                db.Entry(zlecenie).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message.ToString();
                return View();
            }

            return RedirectToAction("Details", new { id = zlId });

        }





        // GET: ZLECENIE/Delete/5
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZLECENIE zLECENIE = db.ZLECENIE.Find(id);
            if (zLECENIE == null)
            {
                return HttpNotFound();
            }
            return View(zLECENIE);
        }

        // POST: ZLECENIE/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles ="Operacje,Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ZLECENIE zLECENIE = db.ZLECENIE.Find(id);
            db.ZLECENIE.Remove(zLECENIE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult AddApps(int zlId)
        {
            ViewBag.ZLID = zlId;
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
