﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace InsTerm.Controllers
{
    [Authorize(Roles ="Instalator,Operacje,Administrator")]
    public class MController : Controller
    {
        private DB_Entities db = new DB_Entities();

        // GET: M
        public ActionResult Index()
        {
            if(User.IsInRole("Instalator"))
            {
                var Merchant = (from m in db.MERCHANT
                                join zl in db.ZLECENIE on m.M_ID equals zl.ZL_MID
                                where zl.ZL_USER == User.Identity.Name
                                select m).ToList();
                return View(Merchant);
            }
            else
            {
                var mERCHANT = db.MERCHANT.OrderBy(m => m.M_NAME).ToList();
                return View(mERCHANT);
            }

        }

        [HttpPost]
        public ActionResult Index(string mid)
        {
            if (String.IsNullOrEmpty(mid))
                return View();

            if (User.IsInRole("Instalator"))
            {
                var merchant = (from m in db.MERCHANT
                                join zl in db.ZLECENIE on m.M_ID equals zl.ZL_MID
                                where zl.ZL_USER == User.Identity.Name
                                    && m.M_NAME.Contains(mid)
                                select m).ToList();
                if (merchant.Count() == 0) return RedirectToAction("Index");
                else if (merchant.Count() == 1) return RedirectToAction("Details", new { id = merchant.ElementAt(0).M_ID });
                else
                {
                    ViewBag.Error = "Nie odnaleziono takiego akceptanta.";
                    return View(merchant);
                }
            }
            else
            {
                var merchant = db.MERCHANT.Where(m => m.M_NAME.Contains(mid)).ToList();
                if (merchant.Count() == 0) return RedirectToAction("Index");
                else if (merchant.Count() == 1) return RedirectToAction("Details", new { id = merchant.ElementAt(0).M_ID });
                else
                {
                    ViewBag.Error = "Nie odnaleziono takiego akceptanta.";
                    return View(merchant);
                }
            }

        }


        // GET: M/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            MERCHANT mERCHANT = db.MERCHANT
                .Where(m => m.M_ID == id)
                .FirstOrDefault();
            if (mERCHANT == null)
            {
                return RedirectToAction("Index");
            }
            return View(mERCHANT);
        }

        // GET: M/Create
        [Authorize(Roles ="Operacje,Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: M/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Create([Bind(Include = "M_ID,M_NAME,M_INFO,M_CITY,M_STREET,M_STREET2,M_EMAIL,M_PHONE1,M_PHONE2")] MERCHANT mERCHANT)
        {
            MERCHANT merchant = db.MERCHANT
                     .Where(m => m.M_ID == mERCHANT.M_ID)
                     .FirstOrDefault();
            if (merchant != null)
                {
                    ViewBag.Error = "Akceptant o podanym numerze MID już istnieje.";
                    return View();
                }
            try
            {
                db.MERCHANT.Add(mERCHANT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View(mERCHANT);
            }


        }

        // GET: M/Edit/5
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            MERCHANT mERCHANT = db.MERCHANT.Where(m => m.M_ID == id).First();
            if (mERCHANT == null)
            {
                ViewBag.Error = "Nie odnaleziono Akceptanta.";
                return RedirectToAction("Index");
            }
            return View(mERCHANT);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Edit([Bind(Include = "M_ID,M_NAME,M_INFO,M_CITY,M_STREET,M_STREET2,M_EMAIL,M_PHONE1,M_PHONE2")] MERCHANT mERCHANT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mERCHANT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mERCHANT);
        }

        // GET: M/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MERCHANT mERCHANT = db.MERCHANT.Find(id);
            if (mERCHANT == null)
            {
                TempData["Error"] = "Nie odnaleziono takiego punktu.";
                return RedirectToAction("Index");
            }
            return View(mERCHANT);
        }

        // POST: M/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Operacje,Administrator")]
        public ActionResult Delete(FormCollection collection)
        {
            int id = int.Parse(collection.Get("M_ID"));

            MERCHANT mERCHANT = db.MERCHANT.Find(id);
            if (mERCHANT == null) return RedirectToAction("Index");

            int count = db.ZLECENIE.Where(z => z.ZL_MID == id).Count();
            if (count != 0)
            {
                TempData["Error"] = "Nie można usunąć punktu! Punkt jest skojarzony z co najmniej jednym zleceniem";
                return RedirectToAction("Index");
            }

            db.MERCHANT.Remove(mERCHANT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
