//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InsTerm
{
    using System;
    using System.Collections.Generic;
    
    public partial class TERMINAL_APP
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TERMINAL_APP()
        {
            this.ZLECENIE = new HashSet<ZLECENIE>();
        }
    
        public int TA_ID { get; set; }
        public string TA_NAME { get; set; }
        public string TA_VERSION { get; set; }
        public string TA_VENDOR { get; set; }
        public Nullable<int> TA_TYPE { get; set; }
        public string TA_DESC { get; set; }
    
        public virtual TERMINAL_APP_TYPES TERMINAL_APP_TYPES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ZLECENIE> ZLECENIE { get; set; }
    }
}
